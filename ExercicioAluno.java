import java.util.Comparator;
import java.util.List;

import javax.print.attribute.standard.Media;

public class ExercicioAluno {

    public static Aluno maiorNome(List<Aluno> alunos){
        sort(alunos, (l,r) -> l.getNome().compareTo(r.getNome()));
        return alunos.get(alunos.size()-1);
    }

    public static int notaMediana(List<Aluno> alunos){
        sort(alunos, (l,r) -> { if(l.getMedia() < r.getMedia()) return -1; 
                                if(l.getMedia() == r.getMedia()) return 0; 
                                return 1;
                              });
        return alunos.get(alunos.size()/2).getMedia();
    }

    public static void printLista(List<Aluno> alunos){
        sort(alunos,(l,r) -> Long.valueOf(l.getId()).compareTo(Long.valueOf(r.getId())));
        alunos.forEach((a) -> { System.out.println(a.toString());
                                });
    }


 private static <T> void sort(List<T> vs, Comparator<T> c){
     //metodo de classificação
 }    

}

class Aluno{

    String nome;
    int media;
    long id;

    public int getMedia(){
        return media;
    } 
    public long getId(){
        return id;
    } 
    
    public String getNome(){
        return nome;
    }
}